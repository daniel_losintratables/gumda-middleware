# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

with open('requirements.txt') as f:
	install_requires = f.read().strip().split('\n')

# get version from __version__ variable in gumda_middleware/__init__.py
from gumda_middleware import __version__ as version

setup(
	name='gumda_middleware',
	version=version,
	description='Gumda Middleware',
	author='Los Intratables',
	author_email='daniel@losintratables.com',
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
