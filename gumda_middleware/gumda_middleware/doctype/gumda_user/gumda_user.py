# -*- coding: utf-8 -*-
# Copyright (c) 2021, Los Intratables and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
# from frappe import auth
import requests
from jinja2 import Template
from frappe.model.document import Document
import json
from frappe.utils.password import get_decrypted_password

class GumdaUser(Document):
	pass



@frappe.whitelist()
def createUser():
	data = json.loads(frappe.request.data)
	user_list = frappe.get_list("Gumda User", filters={"usr":data["usr"]},fields=["name"])
	if len(user_list) > 0:
		return "Ya existe un usuario con esta dirección de correo electrónico"
	else:
		newDoc = frappe.get_doc({
			"doctype": "Gumda User",
			"usr": data["usr"],
			"pwd": data["pwd"],
			"first_name": data["first_name"],
			"last_name": data["last_name"],
			"phone":data["phone"],
		})
		newDoc.insert()
		mail_template = frappe.get_doc("Email Template","Welcome - New Account")
		tm = Template(mail_template.response_html)
		msg = tm.render(doc = newDoc)
		frappe.sendmail(recipients=newDoc.usr,sender="notificaciones@gumda.app",content=msg,subject=mail_template.subject,)
		return newDoc


@frappe.whitelist()
def updatePassword():
	data = json.loads(frappe.request.data)
	user_list = frappe.get_list("Gumda User", filters={"name":data["user_name"]},fields=["name"])
	if len(user_list) > 0:
		doc = frappe.get_doc("Gumda User",data["user_name"])
		doc.pwd = data["pwd"]
		cont = 0
		for i in doc.projects:
			project = frappe.get_doc("Project",i.project)
			url = f'{project.protocol}://{project.domain}/api/resource/User/{doc.usr}'
			api_secret = get_decrypted_password("Project Table", i.name, 'api_secret', False)
			headers = {
				'Content-Type': 'application/json',
				'Authorization': f'token {i.api_key}:{api_secret}'
			}
			data = json.dumps({
				"new_password": data["pwd"]
			})
			response = requests.request("PUT", url, headers=headers, data=data)
			if response.status_code == 200:
				cont = cont + 1
			else:
				return response.status_code
		if cont == len(doc.projects):
			doc.save()
			mail_template = frappe.get_doc("Email Template","Password changed")
			tm = Template(mail_template.response_html)
			msg = tm.render()
			frappe.sendmail(recipients=doc.usr,sender="notificaciones@gumda.app",content=msg,subject=mail_template.subject,)
			return True
		else:
			return "No se actualizaron todas las contraseñas"

	else:
		return "No se encontró el usuario"		


@frappe.whitelist()
def sendEmailPassword(usr):
	user_list = frappe.get_list("Gumda User", filters={"usr":usr},fields=["name"])
	if len(user_list) > 0:
		doc = frappe.get_doc("Gumda User",user_list[0].name)
		mail_template = frappe.get_doc("Email Template","Forgot Password")
		link = f'https://www.gumda.app/restablecer_contrasenia?usr={doc.name}'
		tm = Template(mail_template.response_html)
		msg = tm.render(link = link)
		frappe.sendmail(recipients=doc.usr,sender="notificaciones@gumda.app",content=msg,subject=mail_template.subject,)
		return True
	else:
		return "No existe una dirección de correo así registrada en la plataforma"