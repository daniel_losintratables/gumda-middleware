# -*- coding: utf-8 -*-
# Copyright (c) 2021, Los Intratables and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
import json
from frappe.utils.password import get_decrypted_password

class Project(Document):
	pass

@frappe.whitelist()
def get_projects():
	data = json.loads(frappe.request.data)
	user_list = frappe.get_list("Gumda User", filters={"usr":data["usr"]},fields=["name"])
	users = []
	for i in user_list:
		doc = frappe.get_doc("Gumda User",i.name)
		passwd = get_decrypted_password("Gumda User", doc.name, 'pwd', False)
		if passwd == data["pwd"]:
			projects = []
			for j in doc.projects:
				doc_project = frappe.get_doc("Project",j.project)
				# doc_project.domain = f'{doc_project.protocol}://{doc_project.domain}'
				projects.append(doc_project)
			users.append({"user":doc,"projects":projects})
	return users


@frappe.whitelist()
def verify_existence(doctype, filters):
	projects = frappe.get_list(doctype, filters=filters,fields=["name"])
	return len(projects) > 0

@frappe.whitelist()
def createProject():
	data = json.loads(frappe.request.data)
	if verify_existence("Project",{"abbr":data["project"]["abbr"]}) == True:
		return "Ya existe una empresa con la misma abreviatura"
	else:
		if verify_existence("Project",{"tax_id":data["project"]["tax_id"]}) == True:
			return "Ya existe una empresa con el mismo RUC"
		else:
			newDoc = frappe.get_doc({
				"doctype": "Project",
				"project_name": data["project"]["project_name"],
				"abbr": data["project"]["abbr"],
				"tax_id": data["project"]["tax_id"],
				"protocol": "https",
				"first_route": "/app",
				"sri_password": data["project"]["sri_password"],
				"domain": format_domain(data["project"]["abbr"])
			})
			newDoc.insert()
			user = frappe.get_doc("Gumda User", data["user"]["name"])
			newProjectTable = frappe.get_doc({
				"doctype": "Project Table",
				"project": newDoc.name,
				"parent": user.name,
				"parenttype": "Gumda User",
				"parentfield": "projects"
			})
			newProjectTable.insert()
			user.projects.append(newProjectTable)
			user.save()
			return newDoc

def format_domain(value):
	aux = value
	aux = aux.replace(" ","_")
	aux = aux.replace("ñ","n")
	aux = aux.replace("á","a")
	aux = aux.replace("é","e")
	aux = aux.replace("í","i")
	aux = aux.replace("ó","o")
	aux = aux.replace("ú","u")
	aux = aux.lower()
	return aux + ".gumda.app"